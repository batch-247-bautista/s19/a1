


    



    

// [SECTION] Function Scoping
    // Scope is the accessibility (visibility) of variables within our program.

        // 3 Types of Scope
            // 1. local/block scope
            // 2. global scope
            // 3. function scope

   

        //console.log(functionVar); //results to an error
        //console.log(functionLet);//results to an error
        //console.log(functionConst);//results to an error

// [SECTION] Nested Functions

    // You can create another function inside a function. 

  
        //console.log(nestedName); 
        //nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in.
   

  
    //nestedFunction(); // will result to an error

// [SECTION] Function and Global Scoped Variables

        // Global Scoped Variable
        
       
        //console.log(nameInside); // will result to an error


// [SECTION] Using alert()

    //alert() allows us to show a small window at the top of our browser page to show information to our user. 

        /*
            alert() syntax:
                alert('messageInString')

        */

    
    /*
    Notes on the use of alert():
        - Show only an alert() for short dialogs/messages to the user. 
        - Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.
    */

// [SECTION] Using prompt()

  
        /*
            prompt() Syntax:

            prompt('dialogInString');

        */

        // prompt() returns an empty string when there is no input. And 'null' if the user cancels the propmt().
    

    let userName = prompt("Kindly Enter Your Username ", '');

if (userName === 'Ronald') {

  let pass = prompt('Kindly Enter Your Password', '');

  if (pass === 'Password') {
    let role1 = prompt('Kindly Enter Your Role');

    alert('Welcome back to the class portal, ' + role1 + "!" );
  } else if (pass === '' || pass === null) {
    alert( 'Password Please!' );
  } else {
    alert( 'Wrong password' );
  }

} else if (userName === '' || userName === null) {
  alert( 'Username please!' );
} else {
  alert( "I don't know you" );
} location.reload();

 window. stop()




/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/
        /*array = [71, 70, 73, 74];

        function averageVar(array) {
                let total = 0;
                let count = 0;

                array.forEach(function(item, index) {
                total += item;
                    count++;
                });
                
                console.log('Hello student, your average is:' + (total / count) + '.  The letter equivalent is'  );
                return total / count;
            }
            
            console.log(averageVar(array));
            */

                var studentA = [['A', 71], ['A', 70], ['A', 73], ['A', 74]];

                var Avgmarks = 0;


                for (var i=0; i < studentA.length; i++) {
                Avgmarks += studentA[i][1];
                var avg = (Avgmarks/studentA.length);
                
                }

                //console.log("Average grade: " + (Avgmarks) / studentA.length);


        

                if (avg <= 74){
                  console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentA.length) + '.  The letter equivalent is F.');      
                  } 
                else if (avg <= 79) {
                        console.log("Grade : D"); 
                          } 
                else if (avg <= 84) 
                     {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentA.length) + '.  The letter equivalent is C.'); 
                } else if (avg <= 89) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentA.length) + '.  The letter equivalent is B.'); 
               
                } else if (avg <= 100) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentA.length) + '.  The letter equivalent is A.'); 

                };


                var studentB = [['B', 75], ['B', 75], ['B', 76], ['B', 78]];

                var Avgmarks = 0;

                for (var i=0; i < studentB.length; i++) {
                Avgmarks += studentB[i][1];
                var avg = (Avgmarks/studentB.length);
                }

                //console.log("Average grade: " + (Avgmarks) / studentA.length);

        

                if (avg <= 75){
                  console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentB.length) + '.  The letter equivalent is F.');      
                  } 
                else if (avg <= 79) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentB.length) + '.  The letter equivalent is D.'); 
                          } 
                else if (avg <= 84) 
                     {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentB.length) + '.  The letter equivalent is C.'); 
                } else if (avg < 89) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentB.length) + '.  The letter equivalent is B.'); 
             
                } else if (avg <= 100) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentB.length) + '.  The letter equivalent is A.'); 

                };


                var studentC = [['C', 80], ['C', 81], ['C', 82], ['C', 78]];

                var Avgmarks = 0;

                for (var i=0; i < studentC.length; i++) {
                Avgmarks += studentC[i][1];
                var avg = (Avgmarks/studentC.length);
                }

                //console.log("Average grade: " + (Avgmarks) / studentA.length);

        

                if (avg <= 74){
                  console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentC.length) + '.  The letter equivalent is F.');      
                  } 
                else if (avg < 79) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentC.length) + '.  The letter equivalent is D.'); 
                          } 
                else if (avg <= 84) 
                     {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentC.length) + '.  The letter equivalent is C.'); 
                } else if (avg <= 89) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentC.length) + '.  The letter equivalent is B.'); 
               
                } else if (avg <= 100) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentC.length) + '.  The letter equivalent is A.'); 

                };


                 var studentD = [['D', 84], ['D', 85], ['D', 87], ['D', 88]];

                var Avgmarks = 0;

                for (var i=0; i < studentD.length; i++) {
                Avgmarks += studentD[i][1];
                var avg = (Avgmarks/studentD.length);
                }

                //console.log("Average grade: " + (Avgmarks) / studentA.length);

        

                if (avg <= 74){
                  console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentD.length) + '.  The letter equivalent is F.');      
                  } 
                else if (avg < 79) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentD.length) + '.  The letter equivalent is D.'); 
                          } 
                else if (avg < 84) 
                     {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentD.length) + '.  The letter equivalent is C.'); 
                } else if (avg < 89) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentD.length) + '.  The letter equivalent is B.'); 
                
                } else if (avg < 100) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentD.length) + '.  The letter equivalent is A.'); 

                };


                var studentE = [['E', 89], ['E', 90], ['E', 91], ['E', 90]];

                var Avgmarks = 0;

                for (var i=0; i < studentE.length; i++) {
                Avgmarks += studentE[i][1];
                var avg = (Avgmarks/studentE.length);
                }

                //console.log("Average grade: " + (Avgmarks) / studentA.length);

        

                if (avg <= 74){
                  console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentE.length) + '.  The letter equivalent is F.');      
                  } 
                else if (avg <= 79) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentE.length) + '.  The letter equivalent is D.'); 
                          } 
                else if (avg <= 84) 
                     {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentE.length) + '.  The letter equivalent is C.'); 
                } else if (avg <= 89) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentE.length) + '.  The letter equivalent is B.'); 
               
                } else if (avg < 100) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentE.length) + '.  The letter equivalent is A.'); 

                };

                var studentF = [['F', 91], ['F', 96], ['F', 97], ['F', 95]];

                var Avgmarks = 0;

                for (var i=0; i < studentA.length; i++) {
                    
                Avgmarks += studentF[i][1];

                var avg = (Avgmarks/studentF.length);
                


                }

                //console.log("Average grade: " + (Avgmarks) / studentA.length);
                ;

               
                if (avg <= 74){
                  console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentF.length) + '.  The letter equivalent is F.');

                  } 
                else if (avg <= 79) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentF.length) + '.  The letter equivalent is D.'); 
                          } 
                else if (avg <= 84) 
                     {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentF.length) + '.  The letter equivalent is C.'); 
                } else if (avg <= 89) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentF.length) + '.  The letter equivalent is B.'); 
               
                } else if (avg < 100) {
                        console.log('Hello student, your average is  '  + Math.round((Avgmarks) / studentF.length) + '.  The letter equivalent is A.'); 

                };

        /*if (avg < 75){
          console.log("Grade : F");      
          } 
        else if (avg >= 76 =< 79) {
                console.log("Grade : D"); 
                  } 
        else if (avg >= 80 =< 85 ) 
             {
                console.log("Grade : C"); 
        } else if (avg >= 86 => 90) {
                console.log("Grade : B"); 
        } else if (avg >=91 ,= 95) {
                console.log("Grade : A"); 
        }else if (avg > 95) {
                console.log("Grade : A");

           */





/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.
*/